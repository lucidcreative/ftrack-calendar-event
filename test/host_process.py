import ftrack_api
import os, sys
import logging

# import setup for non-heroku
if not os.path.isdir("/app/.heroku"):
    os.environ.setdefault('IS_HEROKU','False')
    import setup
else:
    os.environ.setdefault('IS_HEROKU','True')
    
dir_path = os.path.dirname(os.path.realpath(__file__))
plugin_path = os.path.realpath(os.path.join(dir_path,'..','plugin_root'))

os.environ['FTRACK_EVENT_PLUGIN_PATH'] = plugin_path

def main():
    
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)
    
    logger.info("Starting up calendar listener....")

    session = ftrack_api.Session()

    session.event_hub.wait()

if __name__ == '__main__':
    main()
